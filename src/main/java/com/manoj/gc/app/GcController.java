package com.manoj.gc.app;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GcController {
	
	@GetMapping("/app")
	public String getGcMessage() {
		return "Welcome Google Cloud";
	}
	
	@GetMapping("/")
	public String test() {
		return "Welcome Google Cloud:defalut";
	}

}
